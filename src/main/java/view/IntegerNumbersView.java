package view;

import static util.Constants.GOODBYE;
import static util.Constants.INTEGER_VIEW;
import static util.Constants.SCANNER;
import static util.NumberGenerator.generateByte;
import static util.NumberGenerator.generateInteger;
import static util.NumberGenerator.generateLong;
import static util.NumberGenerator.generateShort;
import static view.NumberView.chooseType;
import static view.ProcessesIntegerView.chooseIntegerProcesses;

public class IntegerNumbersView {

  public static void chooseIntegers() {
    System.out.println(INTEGER_VIEW);

    final String choice = SCANNER.nextLine();

    switch (choice) {
      case "1":
        chooseIntegerProcesses(generateInteger());
        break;
      case "2":
        chooseIntegerProcesses(generateLong());
        break;
      case "3":
        chooseIntegerProcesses(generateByte());
        break;
      case "4":
        chooseIntegerProcesses(generateShort());
        break;
      case "b":
        chooseType();
      case "q":
        System.out.println(GOODBYE);
        break;
    }
    SCANNER.close();
  }
}

