package view;

import static util.Constants.GOODBYE;
import static util.Constants.PROCESS_INTEGER_VIEW;
import static util.Constants.SCANNER;
import static util.Constants.STARS;
import static view.NumberView.chooseType;

import java.util.List;
import service.DataProcessorService;
import service.impl.DataProcessorServiceImpl;

public class ProcessesIntegerView {

  private static final DataProcessorService dataProcessorService = new DataProcessorServiceImpl();

  public static void chooseIntegerProcesses(final List<?> numberType) {
    System.out.println(PROCESS_INTEGER_VIEW);

    final String choice = SCANNER.nextLine();

    switch (choice) {
      case "1":
        System.out.printf("Count of prime numbers: %s.%n",
            dataProcessorService.countPrimeNumbers(numberType));
        chooseType();
        break;
      case "2":
        choosePositions(numberType);
        chooseType();
        break;
      case "3":
        System.out.printf("Max value: %d %n %n", dataProcessorService.findMaxValue(numberType));
        chooseType();
        break;
      case "b":
        chooseType();
      case "q":
        System.out.println(GOODBYE);
        break;
    }
    SCANNER.close();
  }

  public static void choosePositions(final List<?> numberType) {

    System.out
        .printf("Enter %d position of the number to move in range 0-%d %n", 1, numberType.size()-1);
    final int firstPosition = SCANNER.nextInt();

    System.out
        .printf("Enter %d position where to move the number in range 0-%d%n", 1, numberType.size()-1);
    final int firstPositionToMove = SCANNER.nextInt();

    System.out
        .printf("Enter %d position of the number to move in range 0-%d %n", 2, numberType.size()-1);
    final int secondPosition = SCANNER.nextInt();

    System.out
        .printf("Enter %d position where to move the number in range 0-%d%n", 2, numberType.size()-1);
    final int secondPositionToMove = SCANNER.nextInt();

    System.out.printf("%s Numbers before moving %n %s %n %s %n %n", STARS, numberType, STARS);
    System.out.printf("%s Numbers after moving %n %s %n %s %n %n", STARS,
        dataProcessorService.exchangePosition(numberType, firstPosition,
            firstPositionToMove, secondPosition, secondPositionToMove), STARS);
  }
}
