package view;

import static util.Constants.GOODBYE;
import static util.Constants.NUMBER_VIEW;
import static util.Constants.SCANNER;
import static view.FractionalNumbersView.chooseFractional;
import static view.IntegerNumbersView.chooseIntegers;

public class NumberView {

  public static void chooseType() {
    System.out.println(NUMBER_VIEW);

    final String choice = SCANNER.nextLine();

    switch (choice) {
      case "1":
        chooseIntegers();
        break;
      case "2":
        chooseFractional();
        break;
      case "q":
        System.out.println(GOODBYE);
        return;
    }
    SCANNER.close();
  }
}
