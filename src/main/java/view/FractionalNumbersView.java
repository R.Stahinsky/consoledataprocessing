package view;

import static util.Constants.FRACTIONAL_VIEW;
import static util.Constants.GOODBYE;
import static util.Constants.SCANNER;
import static util.NumberGenerator.generateDouble;
import static util.NumberGenerator.generateFloat;
import static view.NumberView.chooseType;
import static view.ProcessesFractionalView.chooseFractionalProcesses;

public class FractionalNumbersView {

  public static void chooseFractional() {
    System.out.println(FRACTIONAL_VIEW);

    final String choice = SCANNER.nextLine();

    switch (choice) {
      case "1":
        chooseFractionalProcesses(generateDouble());
        break;
      case "2":
        chooseFractionalProcesses(generateFloat());
        break;
      case "b":
        chooseType();
      case "q":
        System.out.println(GOODBYE);
        break;
    }
    SCANNER.close();
  }
}
