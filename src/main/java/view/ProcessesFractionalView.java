package view;

import static util.Constants.GOODBYE;
import static util.Constants.PROCESS_FRACTIONAL_VIEW;
import static util.Constants.SCANNER;
import static view.NumberView.chooseType;
import static view.ProcessesIntegerView.choosePositions;
import java.util.List;
import service.DataProcessorService;
import service.impl.DataProcessorServiceImpl;

public class ProcessesFractionalView {

  private static final DataProcessorService dataProcessorService = new DataProcessorServiceImpl();

  public static void chooseFractionalProcesses(final List<?> numberType){
    System.out.println(PROCESS_FRACTIONAL_VIEW);

    final String choice = SCANNER.nextLine();

    switch (choice) {
      case "1":
        choosePositions(numberType);
        chooseType();
      case "2":
        System.out.printf("Max value: %f %n %n", dataProcessorService.findMaxFractionalValue(numberType));
        chooseType();
      case "b":
        chooseType();
      case "q":
        System.out.println(GOODBYE);
        break;
    }
    SCANNER.close();
  }
}
