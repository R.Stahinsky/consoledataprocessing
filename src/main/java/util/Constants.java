package util;

import java.util.Scanner;

public class Constants {

  public static final String STARS = "***************************";
  public static final String PRESS = "Press";

  public static final String INTEGER_VIEW = String.format("%s %n%s %d to choose Integer %n%s %d "
      + "to choose Long %n%s %d to choose Byte %n%s %d to choose Short %n%s [b] to go back %n%s [q] to quit",
      STARS, PRESS, 1, PRESS, 2, PRESS, 3, PRESS, 4, PRESS, PRESS);
  public static final String FRACTIONAL_VIEW = String.format("%s %n%s %d to choose Double %n%s %d"
      + " to choose Float %n%s [b] to go back %n%s [q] to quit", STARS, PRESS, 1, PRESS, 2, PRESS, PRESS);
  public static final String GOODBYE = String.format("%s Goodbye %s", STARS, STARS);
  public static final String NUMBER_VIEW = String.format("%s %nWelcome to number processor %n%s%n"
      + "Choose type of number that will be processed  %n %n%s %d to choose integer numbers %n%s %d"
      + " to choose fractional numbers", STARS, STARS, PRESS, 1, PRESS, 2);
  public static final String PROCESS_INTEGER_VIEW = String.format("%s %n Choose how to process numbers %n%s %d"
      + " to count the number of elements in a collection %n%s %d"
      + " exchange the positions of two different elements in an array %n%s %d"
      + " find the maximal element in the range of a list %n%s [b] to go back %n%s [q] to quit",
      STARS, PRESS, 1, PRESS, 2, PRESS, 3, PRESS, PRESS);
  public static final String PROCESS_FRACTIONAL_VIEW = String.format("%s %n Choose how to process numbers %n%s %d "
      + "exchange the positions of two different elements in an array %n%s %d"
      + " find the maximal element in the range of a list %n%s [b] to go back %n%s [q] to quit",
      STARS, PRESS, 1, PRESS, 2, PRESS, PRESS);
  public static final Scanner SCANNER = new Scanner(System.in);
}
