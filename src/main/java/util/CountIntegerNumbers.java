package util;

import java.util.List;
import java.util.stream.IntStream;

public class CountIntegerNumbers {

  public static long countIntegerNumbers(final List<Integer> numberType) {
    return numberType.stream()
        .filter(CountIntegerNumbers::isPrime)
        .count();
  }

  public static long countLongNumbers(final List<Long> numberType) {
    return numberType.stream()
        .filter(CountIntegerNumbers::isPrime)
        .count();
  }

  public static long countByteNumbers(final List<Byte> numberType) {
    return numberType.stream()
        .filter(CountIntegerNumbers::isPrime)
        .count();
  }

  public static long countShortNumbers(final List<Short> numberType) {
    return numberType.stream()
        .filter(CountIntegerNumbers::isPrime)
        .count();
  }

  private static boolean isPrime(final int number) {
    return number > 1 && IntStream.range(2, number).noneMatch(i -> number % i == 0);
  }

  private static boolean isPrime(final long number) {
    return number > 1 && IntStream.range(2,
        (int) number).noneMatch(i -> number % i == 0);
  }

  private static boolean isPrime(final byte number) {
    return number > 1 && IntStream.range(2,
        number).noneMatch(i -> number % i == 0);
  }

  private static boolean isPrime(final short number) {
    return number > 1 && IntStream.range(2,
        number).noneMatch(i -> number % i == 0);
  }
}
