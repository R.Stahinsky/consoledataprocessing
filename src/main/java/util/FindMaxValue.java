package util;

import java.util.Comparator;
import java.util.List;

public class FindMaxValue {

  public static long findMaxInteger(final List<Integer> numberType) {
    return numberType.stream()
        .max(Comparator.naturalOrder())
        .get();
  }

  public static long findMaxLong(final List<Long> numberType) {
    return numberType.stream()
        .max(Comparator.naturalOrder())
        .get();
  }

  public static long findMaxByte(final List<Byte> numberType) {
    return numberType.stream()
        .max(Comparator.naturalOrder())
        .get();
  }

  public static long findMaxShort(final List<Short> numberType) {
    return numberType.stream()
        .max(Comparator.naturalOrder())
        .get();
  }
  public static double findMaxDouble(final List<Double> numberType) {
    return numberType.stream()
        .max(Comparator.naturalOrder())
        .get();
  }

  public static double findMaxFloat(final List<Float> numberType) {
    return numberType.stream()
        .max(Comparator.naturalOrder())
        .get();
  }
}

