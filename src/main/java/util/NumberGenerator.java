package util;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NumberGenerator {

  private static final Random Random = new Random();

  public static final List<Integer> generateInteger() {

    return Stream.generate(Random::nextInt)
        .limit(generateBorder())
        .collect(Collectors.toList());
  }

  public static final List<Long> generateLong() {

    return Stream.generate(Random::nextLong)
        .limit(generateBorder())
        .collect(Collectors.toList());
  }

  public static final List<Byte> generateByte() {
    final byte[] bytes = new byte[generateBorder()];
    Random.nextBytes(bytes);

    final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);

    return Stream.generate(() -> (byte) bis.read())
        .limit(bytes.length)
        .collect(Collectors.toList());
  }

  public static final List<Short> generateShort() {

    return Stream.generate(() -> (short) Random.nextInt())
        .limit(generateBorder())
        .collect(Collectors.toList());
  }

  public static final List<Double> generateDouble() {

    return Stream.generate(Random::nextDouble)
        .limit(generateBorder())
        .collect(Collectors.toList());
  }

  public static final List<Float> generateFloat() {

    return Stream.generate(Random::nextFloat)
        .limit(generateBorder())
        .collect(Collectors.toList());
  }

  private static final int generateBorder() {
    return (int) (Math.random() * 100);
  }
}
