package service;

import java.util.List;

public interface DataProcessorService {

  long countPrimeNumbers(final List<?> numberType);

  List<?> exchangePosition(final List<?> numberType, final int firstPosition,
     final int firstPositionToMove, final int secondPosition, final int secondPositionToMove);

  long findMaxValue(final List<?> numberType);

  double findMaxFractionalValue(final List<?> numberType);
}
