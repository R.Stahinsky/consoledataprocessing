package service.impl;

import static util.CountIntegerNumbers.countByteNumbers;
import static util.CountIntegerNumbers.countIntegerNumbers;
import static util.CountIntegerNumbers.countLongNumbers;
import static util.CountIntegerNumbers.countShortNumbers;
import static util.FindMaxValue.findMaxByte;
import static util.FindMaxValue.findMaxDouble;
import static util.FindMaxValue.findMaxFloat;
import static util.FindMaxValue.findMaxInteger;
import static util.FindMaxValue.findMaxLong;
import static util.FindMaxValue.findMaxShort;

import java.util.Collections;
import java.util.List;
import service.DataProcessorService;

public class DataProcessorServiceImpl implements DataProcessorService {

  @Override
  public long countPrimeNumbers(final List<?> numberType) {

    for (final Object number : numberType) {
      if (number.getClass().getName().contains("Integer")) {
        return countIntegerNumbers((List<Integer>) numberType);
      } else {
        if (number.getClass().getName().contains("Long")) {
          return countLongNumbers((List<Long>) numberType);
        } else {
          if (number.getClass().getName().contains("Byte")) {
            return countByteNumbers((List<Byte>) numberType);
          } else {
            return countShortNumbers((List<Short>) numberType);
          }
        }
      }
    }
    return 0;
  }

  @Override
  public List<?> exchangePosition(final List<?> numberType, final int firstPosition,
      final int firstPositionToMove, final int secondPosition, final int secondPositionToMove) {

    Collections.swap(numberType, firstPosition, firstPositionToMove);
    Collections.swap(numberType, secondPosition, secondPositionToMove);

    return numberType;
  }

  @Override
  public long findMaxValue(final List<?> numberType) {
    for (final Object number : numberType) {
      if (number.getClass().getName().contains("Integer")) {
        return findMaxInteger((List<Integer>) numberType);
      } else {
        if (number.getClass().getName().contains("Long")) {
          return findMaxLong((List<Long>) numberType);
        } else {
          if (number.getClass().getName().contains("Byte")) {
            return findMaxByte((List<Byte>) numberType);
          } else {
            return findMaxShort((List<Short>) numberType);
          }
        }
      }
    }
    return 0;
  }

@Override
  public double findMaxFractionalValue(final List<?> numberType) {
    for (final Object number : numberType) {
      if (number.getClass().getName().contains("Double")) {
        return findMaxDouble((List<Double>) numberType);
      } else {
        return findMaxFloat((List<Float>) numberType);
      }
    }
    return 0;
  }
}


