package model;

import java.util.Objects;

public class NumberType<T> {

  private T number;

  public NumberType(T number) {
    this.number = number;
  }

  public NumberType() {
  }

  public T getNumber() {
    return number;
  }

  public void setNumber(T number) {
    this.number = number;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NumberType<?> that = (NumberType<?>) o;
    return Objects.equals(number, that.number);
  }

  @Override
  public int hashCode() {
    return Objects.hash(number);
  }

  @Override
  public String toString() {
    return "Number{" +
        "number=" + number +
        '}';
  }
}
